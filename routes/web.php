<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

Route::get('brands', 'BrandController@index');
Route::get('brands/{brand}', 'BrandController@show');
Route::post('brands', 'BrandController@store');
Route::put('brands/{brand}', 'BrandController@update');
Route::delete('brands/{brand}', 'BrandController@delete');

Route::get('productGroups', 'ProductGroupController@index');
Route::get('productGroups/{pg}', 'ProductGroupController@show');
Route::post('productGroups', 'ProductGroupController@store');
Route::put('productGroups/{pg}', 'ProductGroupController@update');
Route::delete('productGroups/{pg}', 'ProductGroupController@delete');

Route::get('uoms', 'UomController@index');
Route::get('uoms/{uom}', 'UomController@show');
Route::post('uoms', 'UomController@store');
Route::put('uoms/{uom}', 'UomController@update');
Route::delete('uoms/{uom}', 'UomController@delete');

Route::get('products', 'ProductController@index');
Route::get('products/{product}', 'ProductController@show');
Route::post('products', 'ProductController@store');
Route::put('products/{product}', 'ProductController@update');
Route::delete('products/{product}', 'ProductController@delete');