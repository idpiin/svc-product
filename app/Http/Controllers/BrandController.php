<?php

namespace App\Http\Controllers;

use App\Model\Brand;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BrandController extends Controller
{
    use ApiResponser;

    public function __construct()
    {
        //
    }

    public function index()
    {
        $brands = Brand::all();
        return $this->successResponse($brands);
    }

    public function show($brand)
    {
        $brand = Brand::findOrFail($brand);
        return $this->successResponse($brand);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:50'
        ];
        $this->validate($request, $rules);

        $brand = Brand::create($request->all());

        return $this->successResponse($brand, Response::HTTP_CREATED);
    }

    public function update(Request $request, $brand)
    {
        $rules = [
            'name' => 'required|max:50'
        ];
        $this->validate($request, $rules);
        $brand = Brand::findOrFail($brand);

        $brand->fill($request->all());

        if ($brand->isClean()) {
            return $this->errorResponse('At least one value must changed', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $brand->save();

        return $this->successResponse($brand);
    }

    public function delete($brand)
    {
        $brand = Brand::findOrFail($brand);
        $brand->delete();

        return $this->successResponse($brand);
    }
}
