<?php

namespace App\Http\Controllers;

use App\Model\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        return Product::all();
    }

    public function show(Product $pg)
    {
        return $pg;
    }

    public function store(Request $request)
    {
        $pg = Product::create($request->all());

        return response()->json($pg, 201);
    }

    public function update(Request $request, Product $pg)
    {
        $pg->update($request->all());

        return response()->json($pg, 200);
    }

    public function delete(Product $pg)
    {
        $pg->delete();

        return response()->json(null, 204);
    }
}
