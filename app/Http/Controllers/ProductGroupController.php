<?php

namespace App\Http\Controllers;

use App\Model\ProductGroup;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class ProductGroupController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index()
    {
        $productGroups = ProductGroup::all();
        return $this->successResponse($productGroups);
    }

    public function show($pg)
    {
        $productGroup = ProductGroup::findOrFail($pg);
        return $this->successResponse($productGroup);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:50'
        ];
        $this->validate($request, $rules);

        $pg = ProductGroup::create($request->all());

        return $this->successResponse($pg, Response::HTTP_CREATED);
    }

    public function update(Request $request, $pg)
    {
        $rules = [
            'name' => 'required|max:50'
        ];
        $this->validate($request, $rules);
        $pg = ProductGroup::findOrFail($pg);

        $pg->fill($request->all());

        if ($pg->isClean()) {
            return $this->errorResponse('At least one value must changed', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $pg->save();

        return $this->successResponse($pg);
    }

    public function delete($pg)
    {
        $pg = Brand::findOrFail($pg);
        $pg->delete();

        return $this->successResponse($pg);
    }
}
