<?php

namespace App\Http\Controllers;

use App\Model\Uom;
use Illuminate\Http\Request;

class UomController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        return Uom::all();
    }

    public function show(Uom $uom)
    {
        return $uom;
    }

    public function store(Request $request)
    {
        $uom = Uom::create($request->all());

        return response()->json($uom, 201);
    }

    public function update(Request $request, Uom $uom)
    {
        $uom->update($request->all());

        return response()->json($uom, 200);
    }

    public function delete(Uom $uom)
    {
        $uom->delete();

        return response()->json(null, 204);
    }
}
