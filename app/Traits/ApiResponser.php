<?php
/**
 * Created by PhpStorm.
 * User: indra
 * Date: 11/23/2019
 * Time: 2:34 PM
 */

namespace App\Traits;

use Illuminate\Http\Response;

trait ApiResponser
{
    public function successResponse($data, $code = Response::HTTP_OK)
    {
        return \response()->json(['data' => $data], $code);
    }

    public function errorResponse($message, $code)
    {
        return \response()->json(['error' => $message, 'code' => $code], $code);
    }
}