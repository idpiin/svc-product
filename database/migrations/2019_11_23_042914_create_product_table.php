<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('barcode')->nullable();
            $table->text('description')->nullable();
            $table->decimal('buy_price', 25, 2)->nullable();
            $table->decimal('sell_price', 25, 2)->nullable();
            $table->boolean('dynamic_price')->default(false);
            $table->boolean('track_inventory')->default(false);
            $table->boolean('empty_stock')->default(false);
            $table->text('photo')->nullable();
            $table->decimal('stock', 8, 2)->nullable();
            $table->unsignedBigInteger('uom_id');
            $table->unsignedBigInteger('brand_id');
            $table->unsignedBigInteger('product_group_id');

            $table->foreign('uom_id')->references('id')->on('uoms');
            $table->foreign('brand_id')->references('id')->on('brands');
            $table->foreign('product_group_id')->references('id')->on('product_groups');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
