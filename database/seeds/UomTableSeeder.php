<?php

use Illuminate\Database\Seeder;

class UomTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('uoms')->insert(
            [
            'name' => 'kilograms',
            'symbol' => 'kg',
            'description' => 'unit of measure for kilogram',
            ],[
            'name' => 'pounds',
            'symbol' => 'lb',
            'description' => 'unit of measure for pounds',
            ],[
            'name' => 'ons',
            'symbol' => 'oz',
            'description' => 'unit of measure for ons',
            ]
        );
    }
}
