<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Model\ProductGroup::class, 5)->create();
        factory(\App\Model\Brand::class, 5)->create();
        $this->call(UomTableSeeder::class);
        $this->call(ProductTableSeeder::class);
    }
}
