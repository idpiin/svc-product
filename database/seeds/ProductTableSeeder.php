<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands = \App\Model\Brand::all();
        $productGroups = \App\Model\ProductGroup::all();
        $uoms = \App\Model\Uom::all();

        DB::table('products')->insert(
            [
                'name' => \Faker\Factory::create()->streetName,
                'brand_id' => $brands[0]->id,
                'product_group_id' => $productGroups[0]->id,
                'uom_id' => $uoms[0]->id,
            ]
        );
    }
}
